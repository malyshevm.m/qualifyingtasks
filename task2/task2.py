import collections
import xml.etree.cElementTree as ET
import json,sys,os,base64




employeesFile = os.environ.get('EMPLOYEES_FILE')
outputFile = os.environ.get('OUTPUT_FILE')
outputFormat = os.environ.get('OUTPUT_FORMAT')


class Employer(object):
    def __init__(self,pLastName,pFirstName,pEmail,pTitle):
        self.last_name=pLastName
        self.first_name=pFirstName
        self.email=pEmail
        self.title=pTitle

    def __str__(self):
        return self.last_name+" "+self.first_name
    def __eq__(self, other):
        return str(self) == str(other)
    def __gt__(self, other):
        return str(self) > str(other)
#    def __dict__(self):
#        return str(self.title)

class Developer(Employer):
    def __init__(self, pLastName, pFirstName, pEmail, pTitle,pSystems):
        self.last_name=pLastName
        self.first_name=pFirstName
        self.email=pEmail
        self.title=pTitle
        self.systems=pSystems

class SysAdmin(Employer):
    def __init__(self, pLastName, pFirstName, pEmail, pTitle,pVms):
        self.last_name=pLastName
        self.first_name=pFirstName
        self.email=pEmail
        self.title=pTitle
        self.vms=pVms

class Chief(Employer):
    def __init__(self, pLastName, pFirstName, pEmail, pTitle,pDept):
        self.last_name=pLastName
        self.first_name=pFirstName
        self.email=pEmail
        self.title=pTitle
        self.departament=pDept

def getEmployerObject(pEmp):
    if ((pEmp['title']) == 'Разработчик'):
        return  Developer(pEmp['first_name'], pEmp['last_name'], pEmp['email'],pEmp['title'], pEmp['systems'])
    elif ((pEmp['title']) == 'Начальник отдела') or ((pEmp['title']) == 'Начальник'):
         return Chief(pEmp['first_name'], pEmp['last_name'], pEmp['email'],pEmp['title'], pEmp['department'])
    elif ((pEmp['title']) == 'Системный администратор') or ((pEmp['title']) == 'Администратор'):
        return SysAdmin(pEmp['first_name'], pEmp['last_name'], pEmp['email'],pEmp['title'], pEmp['vms'])
    else:
        return Employer(pEmp['first_name'], pEmp['last_name'], pEmp['email'],pEmp['title'])

def parseXML(xml_file):
    with open(xml_file, "r", encoding='utf-8') as read_file:
        tree = ET.ElementTree(file=read_file)
        root = tree.getroot()
        employersCollection =[]
        employers = list(root)
        for employee in employers:
            emp_childrens = list(employee)
            collectEmployer = {}
            collectEmployerDetails = []
            for emp_child in emp_childrens:
                if emp_child.tag in ('systems', 'vms'):
                    details = list(emp_child)
                    for detail in details:
                        collectEmployerDetails.append(detail.text)
                    collectEmployer[emp_child.tag]=collectEmployerDetails
                else:
                    collectEmployer[emp_child.tag]=emp_child.text
            # Добавляем в
            employersCollection.append(getEmployerObject(collectEmployer))

    return employersCollection

def parseJSON(json_file):
    with open(json_file, "r",encoding='utf-8') as read_file:
        data = json.load(read_file)
        employersCollection =[]
        for collectEmployer in data['employees']:
            employersCollection.append(getEmployerObject(collectEmployer))
    return employersCollection

def employeesToList(pEmployees):
    emps = []
    for employee in pEmployees:
        emps.append(employee.__dict__)
    return emps


def saveJSON(pEmployees,pFile):
     emps = {'employees': employeesToList(pEmployees)}
     with open(pFile, "w") as fileForWrite:
        json.dump(emps, fileForWrite, ensure_ascii=False, indent=4)


def createEmpTag(tag, employee):
    rootTag = ET.Element(tag)
    for key, val in employee.items():
        detailTag = ET.Element(key)
        if key == 'systems':
            for detail in val:
                nameTag = ET.Element('name')
                nameTag.text = str(detail)
                detailTag.append(nameTag)
        elif key == 'vms':
            for detail in val:
                ipTag = ET.Element('ip')
                ipTag.text = str(detail)
                detailTag.append(ipTag)
        else:
            detailTag.text = str(val)
        rootTag.append(detailTag)
    return rootTag


def saveXML(pEmployees,pFile):
    finalXml = ET.Element('employees')
    for emp in pEmployees:
        finalXml.append(createEmpTag('employee', emp.__dict__))



    with open(pFile, 'wb') as fileForWrite:
        fileForWrite.write(ET.tostring(finalXml, encoding='utf8', method='html'))


def getObjectsFromFile(path):
    jsonFilename=('EMPLOYEES_FILE.json' if path == None else path+'EMPLOYEES_FILE.json')
    xmlFilename=('EMPLOYEES_FILE.xml' if path == None else path+'EMPLOYEES_FILE.xml')
    if (os.path.exists(jsonFilename)):
        print('Обрабатываем '+jsonFilename)
        return parseJSON(jsonFilename)
    elif (os.path.exists(xmlFilename)):
        print('Обрабатываем '+xmlFilename)
        return parseXML(xmlFilename)
    else:
        return None

def saveObjectsToFile(pEmpArray,pPath,pFormat):

    if (pFormat == None) or (pFormat != 'XML'  and pFormat != 'JSON'):
            print ('Неверно задан формат выходного файла')
            return None

    filePath=('' if pPath == None else pPath)
    if filePath != '':
        if (not os.path.exists(filePath)):
            print('Error!')
            return None
    outFile = str(filePath)+"OUTPUT_FILE."+str(pFormat).lower()

    print('Записываем в ' + outputFormat + ' формат, файл ' + outFile)

    if (pFormat == 'XML'):
        saveXML(pEmpArray, outFile)
    else:
        saveJSON(pEmpArray, "OUTPUT_FILE.json")
    return 1


if __name__ == "__main__":

    empArray=getObjectsFromFile(employeesFile)
    if (empArray == None):
        print ('Файл с исходными данными отсутствует')
        sys.exit(1)


    if (saveObjectsToFile(sorted(empArray,reverse=True),outputFile,outputFormat) == None):
        print('Невозможно создать выходной файл')
