import sys
import collections
import argparse

def readCollection(rdBlocks,fileName):
    rdCollection = collections.Counter()
    f = open(fileName)
    print("Читаем файл '" + parser.parse_args().infile.name + "' блоками по " + str(rdBlocks) + " строк")
    processFile = True
    while processFile:
        for i in range(rdBlocks):
            a = f.readline().strip()
            if not a:
                processFile = False
                break
            rdCollection[int(a)] = rdCollection[int(a)] + 1
    f.close()
    return rdCollection.most_common()

def processCollection(prcCol):
    maxCount = max(dict(prcCol).values())
    for val in sorted(set(dict(prcCol).values()), reverse=True):
        final_dict = sorted({k: v for k, v in dict(prcCol).items() if v == val})
        if len(sorted({k: v for k, v in dict(prcCol).items() if v == val})) == 1 and val == maxCount:
            print(str(final_dict[0]) + " - " + str(val))
        elif len(sorted({k: v for k, v in dict(prcCol).items() if v == val})) != 1:
            print(str(final_dict[0]) + " - " + str(val))


if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    parser.add_argument('infile', nargs='?', type=argparse.FileType('r'))
    parser.add_argument('--size', action='store',type=int)

    if parser.parse_args().infile is None:
        print("Не задано имя файла!")
        sys.exit(1)

    numberBlockForRead = 10;
    if parser.parse_args().size:
        numberBlockForRead = int(parser.parse_args().size)

processCollection(readCollection(numberBlockForRead,parser.parse_args().infile.name))



